insert into vehicleService.services (service_id, user_id, vehicle_id, service_date, description, deleted)
values  (1, 1, 1, '2020-10-14', 'Oil Change', 0),
        (2, 3, 1, '2019-10-03', 'Tyres Change', 0),
        (3, 1, 2, '2019-09-15', 'Engine Check', 0),
        (4, 3, 2, '2020-10-30', 'Brakes Check', 0),
        (5, 1, 1, '2021-10-13', 'Brakes Check', 0),
        (6, 3, 1, '2022-08-03', 'Engine Check', 0),
        (7, 1, 2, '2021-10-20', 'Tyres Change', 0),
        (8, 3, 2, '2022-10-23', 'Oil Change', 0),
        (17, 3, 11, '2021-10-13', 'Brakes Check', 1),
        (18, 3, 12, '2022-08-03', 'Engine Check', 1);

insert into vehicleService.user_roles (user_role_id, role_name)
values  (2, 'owner'),
        (1, 'technician');

insert into vehicleService.users (user_id, e_mail, first_name, last_name, password, role_id, deleted)
values  (1, 'technician1@gmail.com', 'Technician1', 'Technician1', 'technician', 1, 0),
        (2, 'npstaykov@gmail.com', 'Nikolay', 'Staykov', 'npstaykov', 2, 0),
        (3, 'technician2@gmail.com', 'Technician2', 'Technician2', 'technician', 1, 0),
        (4, 'mstaykova@gmail.com', 'Mariana', 'Staykova', 'mstaykova', 2, 0),
        (16, 'TestForDelete@gmail.com', 'OwnerOfTwo', 'OwnerOfTwo', 'owner', 2, 1);

insert into vehicleService.vehicles (vehicle_id, manufacturer, model, vin, reg_no, user_id, deleted)
values  (1, 'Mercedes', 'CLA200', '4Y1SL65848Z411439', 'CA1111CA', 2, 0),
        (2, 'Citroen', 'Xsara', '8Y1SL65848Z411439', 'CA2222CA', 4, 0),
        (11, 'Mercedes', 'SLA', '4Y13335848Z411439', 'CA3333CA', 16, 1),
        (12, 'Citroen', 'Picasso', '8Y1SL6584AAA11439', 'CA4444CA', 16, 1);