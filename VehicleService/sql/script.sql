create table user_roles
(
    user_role_id int auto_increment
        primary key,
    role_name    varchar(30) not null,
    constraint user_roles_role_name_uindex
        unique (role_name),
    constraint user_roles_user_role_id_uindex
        unique (user_role_id)
);

create table users
(
    user_id    int auto_increment
        primary key,
    e_mail     varchar(60)          not null,
    first_name varchar(30)          not null,
    last_name  varchar(30)          not null,
    password   varchar(30)          not null,
    role_id    int                  not null,
    deleted    tinyint(1) default 0 not null,
    constraint users_e_mail_uindex
        unique (e_mail),
    constraint users_user_id_uindex
        unique (user_id),
    constraint users_user_roles_user_role_id_fk
        foreign key (role_id) references user_roles (user_role_id)
);

create table vehicles
(
    vehicle_id   int auto_increment
        primary key,
    manufacturer varchar(30)          not null,
    model        varchar(30)          not null,
    vin          varchar(17)          not null,
    reg_no       varchar(10)          not null,
    user_id      int                  not null,
    deleted      tinyint(1) default 0 not null,
    constraint cars_car_id_uindex
        unique (vehicle_id),
    constraint vehicle_reg_no_uindex
        unique (reg_no),
    constraint vehicle_vin_uindex
        unique (vin),
    constraint vehicle_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table services
(
    service_id   int auto_increment
        primary key,
    user_id      int                  not null,
    vehicle_id   int                  not null,
    service_date date                 not null,
    description  longtext             not null,
    deleted      tinyint(1) default 0 not null,
    constraint services_service_id_uindex
        unique (service_id),
    constraint services_users_user_id_fk
        foreign key (user_id) references users (user_id),
    constraint services_vehicles_vehicle_id_fk
        foreign key (vehicle_id) references vehicles (vehicle_id)
);

