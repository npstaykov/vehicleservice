package com.vehicleservice.repositories;

import com.vehicleservice.exceptions.EntityNotFoundException;
import com.vehicleservice.repositories.contracts.BaseReadRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public abstract class AbstractReadRepository<T> implements BaseReadRepository<T> {

    private final Class<T> clazz;
    protected final SessionFactory sessionFactory;

    public AbstractReadRepository(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    public <V> T getByField(String name, V value) {
        final String query = String.format("from %s where %s = :value", clazz.getName(), name);
        final String notFoundErrorMessage = String.format("%s with %s %s not found", clazz.getSimpleName(), name, value);

        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(query, clazz)
                    .setParameter("value", value)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(notFoundErrorMessage));
        }
    }
    @Override
    public T getById(int id) {
        return getByField("id", id);
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(String.format("from %s ", clazz.getName()) , clazz).list();
        }
    }

}
