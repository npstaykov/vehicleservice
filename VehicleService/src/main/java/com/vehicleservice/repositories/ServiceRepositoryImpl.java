package com.vehicleservice.repositories;

import com.vehicleservice.exceptions.EntityNotFoundException;
import com.vehicleservice.models.Services;
import com.vehicleservice.repositories.contracts.ServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceRepositoryImpl extends AbstractCrudRepository<Services> implements ServiceRepository {

    public ServiceRepositoryImpl(SessionFactory sessionFactory) {
        super(Services.class, sessionFactory);
    }

    @Override
    public List<Services> getServicesForParticularVehicleByTechnician(int userId , int vehicleId){
        try (Session session = sessionFactory.openSession()) {
            Query<Services> query = session.createNativeQuery(
                    "SELECT s.* FROM services s " +
                            "WHERE s.user_id = :userId and s.vehicle_id = :vehicleId ",
                    Services.class
            );
            query.setParameter("userId", userId);
            query.setParameter("vehicleId", vehicleId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Vehicle", "ID" , vehicleId , "User" , "ID" , userId );
            }
            return query.list();
        }
    }
}
