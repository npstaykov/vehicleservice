package com.vehicleservice.repositories.contracts;

import com.vehicleservice.models.UserRole;

public interface UserRoleRepository extends BaseCrudRepository<UserRole>{

}
