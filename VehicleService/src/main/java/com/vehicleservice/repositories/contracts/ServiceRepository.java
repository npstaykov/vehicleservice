package com.vehicleservice.repositories.contracts;

import com.vehicleservice.models.Services;

import java.util.List;

public interface ServiceRepository extends BaseCrudRepository<Services>{

    List<Services> getServicesForParticularVehicleByTechnician(int userId , int vehicleId);

}
