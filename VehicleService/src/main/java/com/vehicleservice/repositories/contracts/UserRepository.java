package com.vehicleservice.repositories.contracts;

import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;

import java.util.List;


public interface UserRepository extends BaseCrudRepository<User>{

    User getByEmail(String email);

    List<Services> getAllServicesByTechnician(int id);

}
