package com.vehicleservice.repositories.contracts;

import com.vehicleservice.models.Services;
import com.vehicleservice.models.Vehicle;

import java.util.List;

public interface VehicleRepository extends BaseCrudRepository<Vehicle>{

    List<Services> getServicesForParticularVehicle(int vehicleId);

}
