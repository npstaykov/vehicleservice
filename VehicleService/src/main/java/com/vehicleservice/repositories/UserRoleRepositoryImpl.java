package com.vehicleservice.repositories;

import com.vehicleservice.models.UserRole;
import com.vehicleservice.repositories.contracts.UserRoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleRepositoryImpl extends AbstractCrudRepository<UserRole> implements UserRoleRepository {

    public UserRoleRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRole.class, sessionFactory);
    }
}
