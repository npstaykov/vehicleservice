package com.vehicleservice.repositories;

import com.vehicleservice.exceptions.EntityNotFoundException;
import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;
import com.vehicleservice.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl extends AbstractCrudRepository<User> implements UserRepository {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where e_mail = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public List<Services> getAllServicesByTechnician(int id){
        try (Session session = sessionFactory.openSession()) {
            Query<Services> query = session.createNativeQuery(
                    "SELECT s.* FROM services s " +
                            "WHERE  s.user_id = :id ORDER BY s.vehicle_id , service_date DESC",
                    Services.class
            );
            query.setParameter("id", id);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Services by" , "User" , "ID"  , String.valueOf(id));
            }
            return query.list();
        }
    }





}

