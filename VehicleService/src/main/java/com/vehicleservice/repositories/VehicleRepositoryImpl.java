package com.vehicleservice.repositories;

import com.vehicleservice.exceptions.EntityNotFoundException;
import com.vehicleservice.models.Services;
import com.vehicleservice.models.Vehicle;
import com.vehicleservice.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class VehicleRepositoryImpl extends AbstractCrudRepository<Vehicle> implements VehicleRepository {

    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        super(Vehicle.class, sessionFactory);
    }

    @Override
    public List<Services> getServicesForParticularVehicle(int vehicleId){
        try (Session session = sessionFactory.openSession()) {
            Query<Services> query = session.createNativeQuery(
                    "SELECT s.* FROM services s " +
                            "WHERE  s.vehicle_id = :vehicleId  order by s.service_date DESC ",
                    Services.class
            );
            query.setParameter("vehicleId", vehicleId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Services for" , "Vehicle" , "ID"  , String.valueOf(vehicleId));
            }
            return query.list();
        }
    }

}
