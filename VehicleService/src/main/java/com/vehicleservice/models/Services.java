package com.vehicleservice.models;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "services", schema = "vehicleService")
public class Services {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "service_id", nullable = false)
    private int serviceId;
    @Basic
    @Column(name = "user_id", nullable = false)
    private int userId;
    @Basic
    @Column(name = "vehicle_id", nullable = false)
    private int vehicleId;
    @Basic
    @Column(name = "service_date", nullable = false)
    private Date serviceDate;
    @Basic
    @Column(name = "description", nullable = false, length = -1)
    private String description;

    @Basic
    @Column(name = "deleted",nullable = false)
    private boolean deleted;

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Date getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(Date serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Services service = (Services) o;
        return serviceId == service.serviceId && userId == service.userId && vehicleId == service.vehicleId && Objects.equals(serviceDate, service.serviceDate) && Objects.equals(description, service.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serviceId, userId, vehicleId, serviceDate, description);
    }
}
