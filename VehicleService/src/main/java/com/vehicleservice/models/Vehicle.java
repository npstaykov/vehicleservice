package com.vehicleservice.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "vehicles", schema = "vehicleService")
public class Vehicle {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "vehicle_id", nullable = false)
    private int vehicleId;
    @Basic
    @Column(name = "manufacturer", nullable = false, length = 30)
    private String manufacturer;
    @Basic
    @Column(name = "model", nullable = false, length = 30)
    private String model;
    @Basic
    @Column(name = "vin", nullable = false, length = 17)
    private String vin;
    @Basic
    @Column(name = "reg_no", nullable = false, length = 10)
    private String regNo;
    @Basic
    @Column(name = "user_id", nullable = false)
    private int userId;

    @Basic
    @Column(name = "deleted",nullable = false)
    private boolean deleted;

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return vehicleId == vehicle.vehicleId && userId == vehicle.userId && Objects.equals(manufacturer, vehicle.manufacturer) && Objects.equals(model, vehicle.model) && Objects.equals(vin, vehicle.vin) && Objects.equals(regNo, vehicle.regNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vehicleId, manufacturer, model, vin, regNo, userId);
    }
}
