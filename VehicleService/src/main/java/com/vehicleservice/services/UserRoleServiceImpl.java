package com.vehicleservice.services;

import com.vehicleservice.models.UserRole;
import com.vehicleservice.repositories.contracts.UserRoleRepository;
import com.vehicleservice.services.contracts.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public List<UserRole> getAll() {
        return userRoleRepository.getAll();
    }
}
