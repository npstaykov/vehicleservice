package com.vehicleservice.services.contracts;

import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;
import com.vehicleservice.models.Vehicle;

import java.util.List;

public interface VehicleService {

    List<Vehicle> getAll();

    Vehicle getById(int id);

    List<Services> getServicesForParticularVehicle(int vehicleId);

    void delete(Vehicle vehicle);

    void softDelete(Vehicle vehicle);
}
