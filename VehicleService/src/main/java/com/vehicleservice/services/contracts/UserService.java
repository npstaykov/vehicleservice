package com.vehicleservice.services.contracts;

import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;
import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByEmail(String email);

    List<Services> getAllServicesByTechnician(int id);

    void create(User user);

    void update(User user, User validUser);

    void delete(int id , User user);

    void softDelete(int id, User user);


}
