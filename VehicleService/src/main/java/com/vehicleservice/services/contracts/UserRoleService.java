package com.vehicleservice.services.contracts;

import com.vehicleservice.models.UserRole;

import java.util.List;

public interface UserRoleService {

    List<UserRole> getAll();
}
