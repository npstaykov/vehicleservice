package com.vehicleservice.services.contracts;

import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;

import java.util.List;

public interface ServiceService {

    List<Services> getAll();

    List<Services> getServicesForParticularVehicleByTechnician(int userId , int vehicleId);

    void delete(Services services);

    void softDelete(Services services);
}
