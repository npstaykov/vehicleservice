package com.vehicleservice.services;

import com.vehicleservice.exceptions.EntityNotFoundException;
import com.vehicleservice.models.Services;
import com.vehicleservice.models.Vehicle;
import com.vehicleservice.repositories.contracts.VehicleRepository;
import com.vehicleservice.services.contracts.ServiceService;
import com.vehicleservice.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final ServiceService serviceService;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository,
                              ServiceService serviceService) {
        this.vehicleRepository = vehicleRepository;
        this.serviceService = serviceService;
    }

    @Override
    public List<Vehicle> getAll() {
        return vehicleRepository.getAll();
    }

    @Override
    public Vehicle getById(int id) {
        try {
            return vehicleRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Vehicle", id);
        }
    }

    @Override
    public List<Services> getServicesForParticularVehicle(int vehicleId){
        return vehicleRepository.getServicesForParticularVehicle(vehicleId);
    }

    @Override
    public void delete(Vehicle vehicle) {
        List<Services> services = getServicesForParticularVehicle(vehicle.getVehicleId());
        for (Services service : services) {
            serviceService.delete(service);
        }
        vehicleRepository.delete(vehicle.getVehicleId());
    }

    @Override
    public void softDelete(Vehicle vehicle) {
        List<Services> services = getServicesForParticularVehicle(vehicle.getVehicleId());
        for (Services service : services) {
            serviceService.softDelete(service);
        }
        vehicle.setDeleted(true);
        vehicleRepository.update(vehicle);

    }

}
