package com.vehicleservice.services;

import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;
import com.vehicleservice.repositories.contracts.ServiceRepository;
import com.vehicleservice.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository serviceRepository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @Override
    public List<Services> getAll() {
        return serviceRepository.getAll();
    }

    @Override
    public List<Services> getServicesForParticularVehicleByTechnician(int userId , int vehicleId){
        return  serviceRepository.getServicesForParticularVehicleByTechnician(userId,vehicleId);
    }

    @Override
    public void delete(Services services) {
        serviceRepository.delete(services.getServiceId());
    }

    @Override
    public void softDelete(Services services) {
        services.setDeleted(true);
        serviceRepository.update(services);
    }

}
