package com.vehicleservice.services;

import com.vehicleservice.exceptions.DuplicateEntityException;
import com.vehicleservice.exceptions.EntityNotFoundException;
import com.vehicleservice.exceptions.UnauthorizedOperationException;
import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;
import com.vehicleservice.models.Vehicle;
import com.vehicleservice.repositories.contracts.UserRepository;
import com.vehicleservice.services.contracts.UserService;
import com.vehicleservice.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Set;


@Service
public class UserServiceImpl implements UserService {

    public static final String ONLY_TECHNICIANS_CAN_CREATE = "Only technicians can create users";
    public static final String ONLY_TECHNICIANS_CAN_DELETE_USER = "Only technicians can delete user";
    public  static final  String ONLY_ACCOUNT_OWNERS_CAN_UPDATE = "Only account owners can update user";
    private final UserRepository userRepository;

    private final VehicleService vehicleService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           VehicleService vehicleService) {
        this.userRepository = userRepository;
        this.vehicleService = vehicleService;
    }


    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        try {

            return userRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
    }

    @Override
    public List<Services> getAllServicesByTechnician(int id){
        return userRepository.getAllServicesByTechnician(id);
    }
    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }


    @Override
    public void create(User user) {
        boolean duplicateEmailExists = true;
        try {
            userRepository.getByEmail(user.geteMail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("User", "email", user.geteMail());
        }
        userRepository.create(user);
    }

    @Override
    public void update(User user , User validUser) {
        if (user.getUserId() != validUser.getUserId()) {
            throw new UnauthorizedOperationException(ONLY_ACCOUNT_OWNERS_CAN_UPDATE);
        }
        userRepository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        if(!user.isTechnician()) {
            throw new UnauthorizedOperationException(ONLY_TECHNICIANS_CAN_DELETE_USER);
        }
        try {
            userRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
        User userToDelete = userRepository.getById(id);
        Set<Vehicle> vehicles = userToDelete.getVehicles();
        for (Vehicle vehicle : vehicles){
            vehicleService.delete(vehicle);
        }
        userRepository.delete(id);
    }

    @Override
    public void softDelete(int id, User user) {
        if(!user.isTechnician()) {
            throw new UnauthorizedOperationException(ONLY_TECHNICIANS_CAN_DELETE_USER);
        }
        try {
            userRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
        User userToSoftDelete = userRepository.getById(id);
        Set<Vehicle> vehicles = userToSoftDelete.getVehicles();
        for (Vehicle vehicle : vehicles){
            vehicleService.softDelete(vehicle);
        }
        userToSoftDelete.setDeleted(true);
        userRepository.update(userToSoftDelete);
    }






}
