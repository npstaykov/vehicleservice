package com.vehicleservice.mappers;

import com.vehicleservice.models.User;
import com.vehicleservice.models.UserRole;
import com.vehicleservice.models.dtos.UserDto;
import com.vehicleservice.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final UserService userService;

    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();

        user.seteMail(userDto.geteMail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPassword(userDto.getPassword());

        UserRole userRole = new UserRole();
        userRole.setUserRoleId(2);

        user.setRole(userRole);

        user.setDeleted(false);

        return user;
    }


    public User toDto(UserDto userDto, User userToUpdate) {

        User user = userService.getById(userToUpdate.getUserId());

        user.seteMail(userDto.geteMail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPassword(userDto.getPassword());

        return user;
    }
}
