package com.vehicleservice.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
    }

    public DuplicateEntityException(String user, int userId) {
        super(String.format("%s with %d already exists.", user , userId));
    }

    public DuplicateEntityException(String s) {

    }
}