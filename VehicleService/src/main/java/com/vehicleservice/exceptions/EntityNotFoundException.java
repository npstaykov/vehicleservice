package com.vehicleservice.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found.", type, attribute, value));
    }

    public EntityNotFoundException(String additional, String type, String attribute, String value) {
        super(String.format("%s %s with %s %s not found.", additional, type, attribute, value));
    }

    public EntityNotFoundException(String type, String attribute, int value,
                                   String typeTwo, String attributeTwo, int valueTwo) {
        super(String.format("%s with %s %s serviced by %s with %s %s not found.", type, attribute, value, typeTwo, attributeTwo, valueTwo));
    }

    public EntityNotFoundException(String notFoundErrorMessage) {
    }
}


