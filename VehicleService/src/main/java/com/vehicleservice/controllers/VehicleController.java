package com.vehicleservice.controllers;

import com.vehicleservice.exceptions.EntityNotFoundException;
import com.vehicleservice.exceptions.UnauthorizedOperationException;
import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;
import com.vehicleservice.models.Vehicle;
import com.vehicleservice.services.contracts.UserService;
import com.vehicleservice.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;

    @Autowired
    public VehicleController(VehicleService vehicleService,
                             AuthenticationHelper authenticationHelper,
                             UserService userService) {
        this.vehicleService = vehicleService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @GetMapping
    public List<Vehicle> getAll() {
        return vehicleService.getAll();
    }


    @GetMapping("/{id}")
    public Vehicle getById(@PathVariable int id) {
        try {
            return vehicleService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{vehicleId}/services")
    public List<Services> getServicesForParticularVehicle(@PathVariable int vehicleId){
        try {
            return vehicleService.getServicesForParticularVehicle(vehicleId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User validUser = authenticationHelper.tryGetUser(headers);
            Vehicle vehicle = vehicleService.getById(id);
            vehicleService.delete(vehicle);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED , e.getMessage());
        }
    }

}
