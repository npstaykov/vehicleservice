package com.vehicleservice.controllers;

import com.vehicleservice.exceptions.DuplicateEntityException;
import com.vehicleservice.exceptions.EntityNotFoundException;
import com.vehicleservice.exceptions.UnauthorizedOperationException;
import com.vehicleservice.mappers.UserMapper;
import com.vehicleservice.models.Services;
import com.vehicleservice.models.User;
import com.vehicleservice.models.dtos.UserDto;
import com.vehicleservice.services.contracts.ServiceService;
import com.vehicleservice.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

import static com.vehicleservice.services.UserServiceImpl.ONLY_TECHNICIANS_CAN_CREATE;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final ServiceService serviceService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService,
                          ServiceService serviceService,
                          UserMapper userMapper,
                          AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.serviceService = serviceService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }


    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/services/{vehicleId}")
    public List<Services> getServicesByVehicle(@PathVariable int id,
                                               @PathVariable int vehicleId) {
        try {
            return serviceService.getServicesForParticularVehicleByTechnician(id, vehicleId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/services")
    public List<Services> getServicesByVehicle(@PathVariable int id) {
        try {
            return userService.getAllServicesByTechnician(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/register")
    public UserDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody UserDto userDto) {
        try {
            User validUser = authenticationHelper.tryGetUser(headers);
            if(!validUser.isTechnician()) {
                throw new UnauthorizedOperationException(ONLY_TECHNICIANS_CAN_CREATE);
            }
            User user = userMapper.fromDto(userDto);
            userService.create(user);
            return userDto;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserDto update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDto userDto) {
        try {
            User validUser = authenticationHelper.tryGetUser(headers);
            User userToUpdate = userService.getById(id);
            User user = userMapper.toDto(userDto, userToUpdate);
            userService.update(user, validUser);
            return userDto;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }



    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User validUser = authenticationHelper.tryGetUser(headers);
            userService.delete(id, validUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED , e.getMessage());
        }
    }

    @DeleteMapping("/{id}/softdelete")
    public void softDelete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User validUser = authenticationHelper.tryGetUser(headers);
            userService.softDelete(id, validUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED , e.getMessage());
        }
    }


}
